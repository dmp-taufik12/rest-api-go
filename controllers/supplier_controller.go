package controllers

import (
	"net/http"
	"github.com/gin-gonic/gin"
	"rest-api-golang/config"
	"rest-api-golang/models"
	// "fmt"
)

func FindSupplierAll(c *gin.Context){
	var suplier []models.Suplier
	config.DB.Find(&suplier)

	c.JSON(http.StatusOK, gin.H{"data": suplier})
}

func FindSupplier(c *gin.Context) {
	var suplier models.Suplier
	if err := config.DB.Where("idsuplier = ?", c.Param("id")).First(&suplier).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": suplier})
}

func CreateSupplier(c *gin.Context) {
	// validasi input
	var input models.CreateSupplierInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	supplier := models.Suplier{NamaSuplier: input.NamaSuplier, Alamat: input.Alamat, Telepon: input.Telepon }
	config.DB.Create(&supplier)

	c.JSON(http.StatusOK, gin.H{
		"Message": "Data Berhasil Disimpan.",
	})
}

func UpdateSupplier(c *gin.Context) {
	var suplier models.Suplier
	if err := config.DB.Where("idsuplier = ?", c.Param("id")).First(&suplier).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var input models.UpdateSupplierInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	supplierUpdate :=  models.Suplier{NamaSuplier: input.NamaSuplier, Alamat: input.Alamat, Telepon: input.Telepon }
	config.DB.Model(&suplier).Updates(supplierUpdate)

	c.JSON(http.StatusOK, gin.H{"Message": "update data sukses"})
}

func DeleteSupplier(c *gin.Context) {
	var suplier models.Suplier
	if err := config.DB.Where("idsuplier = ?", c.Param("id")).First(&suplier).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	config.DB.Delete(&suplier)
	c.JSON(http.StatusOK, gin.H{"message": "data berhasil dihapus"})
}
