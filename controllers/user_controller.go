package controllers

import (
	"net/http"
	"github.com/gin-gonic/gin"
	"rest-api-golang/config"
	"rest-api-golang/models"
	"rest-api-golang/helpers"
	//"encoding/hex"
	//"fmt"
)

type createUser struct {
	NamaLengkap string  `json:"namaLengkap" binding:"required"`
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func Register(c *gin.Context){
	var inputUser createUser
	if err := c.ShouldBindJSON(&inputUser); err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	hashPassword, _ := helpers.HashPassword(inputUser.Password)
	saveUser := models.User{NamaLengkap: inputUser.NamaLengkap, Username: inputUser.Username, Password: hashPassword}
	config.DB.Create(&saveUser)

	c.JSON(http.StatusOK, gin.H{
		"success" : true,
		"message" : "User berhasil disimpan",
	})

}