package controllers

import (
	"net/http"
	"github.com/gin-gonic/gin"
	"rest-api-golang/models"
	//"rest-api-golang/models/views"
	"rest-api-golang/config"
	"github.com/dgrijalva/jwt-go"
	"rest-api-golang/helpers"
	"os"
	"time"
)

func AuthUser(c *gin.Context){
	var user models.User

	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	password := user.Password
	if err := config.DB.Where("username =?", user.Username).First(&user).Error; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": "username tidak cocok"})
		return
	}

	passwordold := user.Password
	cekPassword := helpers.CheckPasswordHash(password, passwordold)
	if cekPassword != true {
		c.JSON(400, gin.H{"error" : "Username dan password tidak cocok"})
		return
	}

    //buat jwt token
	token, err := CreateToken(user.ID, user.NamaLengkap)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}
	//
	var userJson = map[string]interface{}{
		"username" : user.Username,
		"namalengkap" : &user.NamaLengkap,
		"id" : user.ID,
	}
	c.JSON(http.StatusOK, gin.H{
		"succes" : true,
		"user" : userJson,
		"token" : token,
	})

}

func CreateToken(userId uint, namaLengkap string) (string, error) {
	var err error
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = userId
	atClaims["nama_lengkap"] = namaLengkap
	atClaims["exp"] = time.Now().Add(time.Minute * 15).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return "", err
	}
	return token, nil
}







