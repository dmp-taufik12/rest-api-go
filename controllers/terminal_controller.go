package controllers

import (
	"net/http"
	"github.com/gin-gonic/gin"
	"rest-api-golang/config"
	"rest-api-golang/models/views"
	//"fmt"
)

// join dengan table suplier
func FindTerminalAll(c *gin.Context){
	var terminal []views.Terminal
	//listTerminal := []map[string] interface{}{}
	if err :=config.DB.Select("terminals.nama_terminal, supliers.nama_suplier, supliers.alamat, supliers.telepon").
		Joins("INNER JOIN supliers on terminals.suplier_id = supliers.idsuplier").Find(&terminal).Error; err != nil {
			c.JSON(400, gin.H{"error" : "data not found"})
			return
	}
	c.JSON(http.StatusOK, gin.H{
		"data" : terminal,
	})
}

// menampilkan berdasarkan suplierId dengan join table suplier

func FindTerminalBySupliplierId(c *gin.Context)  {
  	var terminal []views.Terminal
  	if err := config.DB.Select("terminals.nama_terminal, supliers.nama_suplier, supliers.alamat, supliers.telepon").
		Joins("LEFT JOIN supliers on terminals.suplier_id = supliers.idsuplier").
  		Where("suplier_id =?", c.Param("id")).
  		Find(&terminal).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
			return
	}
		c.JSON(http.StatusOK, gin.H{
			"data" : terminal,
		})
}