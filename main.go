package main

import (
	"rest-api-golang/config"
	"rest-api-golang/controllers"

	"github.com/gin-gonic/gin"
	//"fmt"
	//"time"
	//"sync"
)

////async
//var async sync.WaitGroup
//
//func kirimbuku(isibuku string)  {
//	for i := 0; i <= 5 ; i++{
//		fmt.Printf("judul buku :%s\n",isibuku)
//		time.Sleep(time.Millisecond * 100)
//	}
//	async.Done()
//}
//
//func mobil(isimobil string)  {
//	for i := 0; i <= 5 ; i++{
//		fmt.Printf("merek mobil :%s\n",isimobil)
//		time.Sleep(time.Millisecond * 100)
//	}
//	//defer async.Done()
//}

func main() {

	////async.Add(1)
	//	kirimbuku("belajar tajwid ")
	//
	//	kirimbuku("belajar tajwid ")
	////async.Wait()
	//
	////async.Add(1)
	//	mobil("Pajero ")
	////async.Wait()

	router := gin.Default()
	config.ConnectDatabase()

	//Routes
	routerGroup := router.Group("/api")
	{

		routerGroup.POST("/auth", controllers.AuthUser)
		routerGroup.POST("/register", controllers.Register)

		routerGroup.GET("/supplier", controllers.FindSupplierAll)
		routerGroup.GET("/supplier/:id", controllers.FindSupplier)
		routerGroup.POST("/supplier", controllers.CreateSupplier)
		routerGroup.PATCH("/supplier/:id", controllers.UpdateSupplier)
		routerGroup.DELETE("/supplier/:id", controllers.DeleteSupplier)

		// terminal
		routerGroup.GET("/terminal", controllers.FindTerminalAll)
		routerGroup.GET("/terminal/:id", controllers.FindTerminalBySupliplierId)
	}

	router.Run()
}
