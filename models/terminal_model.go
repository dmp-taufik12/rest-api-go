package models

type Terminal struct {
	Idterminal   uint   `json:"idterminal" gorm:"primary_key"`
	NamaTerminal string `json:"namaterminal"`
	SuplierId    uint
}
