package models

type User struct {
	ID uint `json:"id" gorm:"primary_key"`
	NamaLengkap  string `json:"namalengkap"`
	Username  string `json:"username"`
	Password  string `json:"password"`

}
