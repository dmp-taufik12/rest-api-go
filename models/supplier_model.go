package models

type CreateSupplierInput struct {
	NamaSuplier string `json:"namasuplier" binding:"required"`
	Alamat string `json:"alamat" binding:"required"`
	Telepon string `json:"telepon" binding:"required"`
}

type UpdateSupplierInput struct {
	NamaSuplier string `json:"namasuplier"`
	Alamat string `json:"alamat"`
	Telepon string `json:"telepon"`

}

type Suplier struct {
	Idsuplier uint `json:"idsuplier" gorm:"primary_key"`
	NamaSuplier  string `json:"namasuplier"`
	Alamat string `json:"alamat"`
	Telepon string `json:"telepon"`
}
